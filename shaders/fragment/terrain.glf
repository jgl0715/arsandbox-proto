#version 330 core

out vec4 frag_color;

in vec4 vert_color;

void main()
{
	
	vec4 low_color = vec4(0.0, 0.0, 1.0, 1.0);
	vec4 high_color = vec4(1.0, 0.0, 0.0, 1.0);
	
	
	float value = gl_FragCoord.z;
	
	// How many topography lines
	int lines = 9;
	vec4 colors[9];
	colors[0] = vec4(0.0, 0.0, 1.0, 1.0);
	colors[1] = vec4(0.004, 0.196, 0.125, 1.0);
	colors[2] = vec4(0.75, 1.0, 0.0, 1.0);
	colors[3] = vec4(1.0, 1.0, 0.0, 1.0);
	colors[4] = vec4(1.0, 0.5, 0.0, 1.0);
	colors[5] = vec4(0.588, 0.294, 0.0, 1.0);
	colors[6] = vec4(0.396, 0.263, 0.129, 1.0);
	colors[7] = vec4(0.8, 0.8, 0.8, 1.0);
	colors[8] = vec4(1.0, 1.0, 1.0, 1.0);

	float threshold = 0.02f;
	float scaled_value = value * lines;
	float line1 = 1 - (floor(scaled_value + threshold) - floor(scaled_value));
	float line2 = 1 + (ceil(scaled_value - threshold) - ceil(scaled_value));
	float c_f = floor(scaled_value);
	int c_i = int(c_f);
	
	frag_color = colors[c_i];
	frag_color *= line1;
	frag_color *= line2;
}