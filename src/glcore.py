from array import array
from pygame.locals import *
from OpenGL.GL import *
from OpenGL.GLU import *
import numpy
import ctypes
from util import *

VERT_DIR = "../shaders/vertex"
FRAG_DIR = "../shaders/fragment"

class VertexAttrib:

	def __init__(self):
		self.name = "none"
		self.size = 0
		self.index = 0
		self.stride = 0
		self.offset = 0

class VertexBuffer:
	
	def __init__(self):
		self.handle = glGenBuffers(1)
		self.buffer_hint = GL_STATIC_DRAW
		self.primitive = GL_TRIANGLES
		self.attribs = []
		self.size = 0
		
	def delete(self):
		glDeleteBuffers(1, self.handle)
		
	def bind(self):
		glBindBuffer(GL_ARRAY_BUFFER, self.handle)
			
	def push_attrib(self, name, size):
		new_attrib = VertexAttrib()
		new_attrib.name = name
		new_attrib.size = size
		new_attrib.index = len(self.attribs)
		
		# Calculate this attrib's stride.
		for x in self.attribs:
			new_attrib.offset += x.size * 4
					
		# Add this attribute.
		self.attribs.append(new_attrib)

		# Set the new stride of all of the vertex attributes
		new_stride = new_attrib.offset + 4 * size
		for x in self.attribs:
			x.stride = new_stride
	
	def pop_attrib(self):
		
		top = self.attribs.pop()
		
		new_stride = top.offset
		# Set the new stride of all of the vertex attributes
		for x in self.attribs:
			x.stride = new_stride
			
	def push_data(self, data, verts):
		self.bind()
		glBufferData(GL_ARRAY_BUFFER, 4 * len(data), data.tobytes(), self.buffer_hint)
		self.size = verts
		
		# Setup vertex attribute specifications.
		for x in self.attribs:
			glVertexAttribPointer(x.index, x.size, GL_FLOAT, GL_FALSE, x.stride, ctypes.c_void_p(x.offset))
			
	def render_indexed(self, index_buffer):
		self.bind()
		index_buffer.bind()
		
		for x in self.attribs:
			glEnableVertexAttribArray(x.index)
			
		# Draws n triangles with this vertex buffer object
		glDrawElements(self.primitive, index_buffer.size, GL_UNSIGNED_INT, None)	
		
		for x in self.attribs:
			glDisableVertexAttribArray(x.index)

		glBindBuffer(GL_ARRAY_BUFFER, 0)
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0)
		

	def render(self):
		self.bind()
		
		for x in self.attribs:
			glEnableVertexAttribArray(x.index)
			
		# Draws n triangles with this vertex buffer object
		glDrawArrays(self.primitive, 0, self.size)	
		
		for x in self.attribs:
			glDisableVertexAttribArray(x.index)

		glBindBuffer(GL_ARRAY_BUFFER, 0)
		
class IndexBuffer:
	
	def __init__(self):
		self.handle = glGenBuffers(1)
		self.buffer_hint = GL_STATIC_DRAW 
		self.size = 0
		
	def delete(self):
		glDeleteBuffers(1, self.handle)
		
	def bind(self):
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, self.handle)
			
	def push_data(self, data):
		self.bind()
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, 4 * len(data), data.tobytes(), GL_STATIC_DRAW)
		self.size = len(data)
	
class Shader:

	# Called when this shader is constructed.
	def __init__(self, vert_shader, frag_shader):
		self._vert_shader = vert_shader
		self._frag_shader = frag_shader
		self._program_handle = None # Handle to the OpenGL shader program.
		
	# Called when this shader is deleted.
	def delete(self):
	
		print("Deleting shader: %d" % (self._program_handle))
		
		glDeleteProgram(self._program_handle)
		

	# Invoke this function when the shader program should be loaded.
	# This loads the source, compiles the source, and links the program together.
	def create(self):
		self._program_handle = glCreateProgram()
		
		# Create mandatory vertex and fragment shaders.
		vert_handle = self.__create_shader(GL_VERTEX_SHADER, VERT_DIR, self._vert_shader)
		frag_handle = self.__create_shader(GL_FRAGMENT_SHADER, FRAG_DIR, self._frag_shader)
		
		# Attach shaders to main program
		glAttachShader(self._program_handle, vert_handle)
		glAttachShader(self._program_handle, frag_handle)
		
		# Link program
		glLinkProgram(self._program_handle)
		
		self.__program_status_check(GL_LINK_STATUS, 'link')
		
		# Validate program
		glValidateProgram(self._program_handle)
		
		# Get rid of the old shader resources.
		glDetachShader(self._program_handle, vert_handle)
		glDetachShader(self._program_handle, frag_handle)
		glDeleteShader(vert_handle)
		glDeleteShader(frag_handle)

		self.__program_status_check(GL_VALIDATE_STATUS, 'link')
		
	def uniform_mat44(name, mat):
		loc = glGetUniformLocation(self._program_handle, name)
		glUniformMatrix3fv(loc, 16, GL_FALSE, mat)

	def bind(self):
		glUseProgram(self._program_handle)
		
	def bind_vbo(self, vbo):		
		for x in vbo.attribs:
			glBindAttribLocation(self._program_handle, x.index, x.name)	
	
	def unbind(self):
		glUseProgram(0)
		
	def __create_shader(self, type, path, name):
		# Create shader handle
		handle = glCreateShader(type)
		# Read source code from directory and file
		source = read_file(path, name)
		# Upload shader source
		glShaderSource(handle, source)
		# Compile shader source
		glCompileShader(handle)
		
		status = glGetShaderiv(handle, GL_COMPILE_STATUS)
		
		if(status == GL_FALSE):
			raise Exception("Shader compilation error in %s: \n %s" % (name, glGetShaderInfoLog(handle)))		
		
		return handle
		
	def __program_status_check(self, type, name):
		status = glGetProgramiv(self._program_handle, type)
		if(status == GL_FALSE):
			raise Exception("GLSL program %s error: \n %s" % (name, glGetProgramInfoLog(self._program_handle)))		
		