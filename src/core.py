import pygame
from pygame.locals import *
from OpenGL.GL import *

class Driver:

	def __init__(self, resolution, app):
		self.app = app
		self.resolution = resolution
		self.gameClock = pygame.time.Clock()
		self.running = False
			
	def start(self):
	
		self.running = True
		
		# Initialize PyGame.
		pygame.init()
				
		# Initialize the display.
		pygame.display.set_mode(self.resolution, DOUBLEBUF | OPENGL)		
		
		glEnable(GL_DEPTH_TEST)
		
		self.app.create()
		
		while self.running:
			self.update()
			self.render()
			
		# Shutdown the app
		self.app.quit()
			
		# Shutdown the system
		pygame.quit()
		quit()
			
	def stop(self):
		self.running = False

	def update(self):
		# Lock to 60 FPS
		self.gameClock.tick(120)
		
		# Parse pygame events.
		for event in pygame.event.get():
			if event.type == pygame.QUIT:
				self.stop()
				
		# Update the user's app.
		self.app.update()	
		
	def render(self):
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
		glClearColor(0.0, 0.0, 0.0, 1.0)
		
		# Render the user's app.
		self.app.render()
		
		pygame.display.flip()
		pygame.time.wait(1)