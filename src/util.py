from array import array

def GenerateTriangle(color):

	generated_tri = array("f")
	verts = 3
	vert_size = 3 # components in a vert (x, y, z)
	
	bottom_left = [-1, -1, 0]
	bottom_right = [-1, 1, 0]
	top = [1, -1, 0]
	
	for x in bottom_left:
		generated_tri.append(x)
	for x in color:
		generated_tri.append(x)
		
	for x in bottom_right:
		generated_tri.append(x)	
	for x in color:
		generated_tri.append(x)
		
	for x in top:
		generated_tri.append(x)		
	for x in color:
		generated_tri.append(x)
			
	return generated_tri

def read_file(path, file_name):
	file_contents = ""
	with open(path + "/" + file_name, 'r') as file:
		file_contents = file.read()	
	return file_contents
