from glcore import *
from core import *
import util
from array import array
from mesh import *
import random
import noise

def make_plane(mesh, res, nx, ny):

	shape = (res, res)
	scale = 10.0
	octaves = 1
	persistence = 1.0
	lacunarity = 1.0

	# Make vertices
	for x in range(res + 1):
		x_c = -1.0 + x * (2.0 / res)
		for y in range(res + 1):
			# random z
			
			x_p = float(x) / (res + 1)
			y_p = float(y) / (res + 1)
			
			y_c = -1.0 + y * (2.0 / res)
			z_c = (noise.pnoise2(float(nx) + x / scale, 
                                    float(ny) + y / scale, 
                                    octaves, 
                                    persistence, 
                                    lacunarity, 
                                    1024, 
                                    1024, 
                                    0) + 1) / 2.0
																		
			rand_color = [random.uniform(0, 1), random.uniform(0, 1), random.uniform(0, 1), 1.0]
			vert = Vertex([x_c, y_c, z_c], rand_color)
			mesh.add_vertex(vert)
			
	# Make indices for faces
	for x in range(res):
		for y in range(res):
			index_top_left = y * (res+1) + x
			index_top_right = y * (res+1) + x + 1
			index_bottom_left = (y+1) * (res+1) + x
			index_bottom_right = (y+1) * (res+1) + x + 1
			
			# Add the two triangles for the square face
			mesh.add_face(index_top_left, index_top_right, index_bottom_right)
			mesh.add_face(index_top_left, index_bottom_right, index_bottom_left)
	
	mesh.update()

class App:

	def __init__(self):
		self.time = 0
		self.mesh = Mesh()
		self.mesh.shader = Shader("terrain.glv", "terrain.glf")
		
	def create(self):		
								
		# Create terrain shader.
		self.mesh.shader.create()
		self.mesh.create()
					
	def update(self):
		# Construct a plane mesh
		self.time += 0.01
		self.mesh.clear()
		make_plane(self.mesh, 30, self.time, self.time)

		return
		
	def render(self):
		#glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
		self.mesh.render()
		
	def quit(self):
		# Delete shader and mesh
		self.mesh.shader.delete()
		self.mesh.delete()

app = App()
driver = Driver((800, 600), app)
driver.start()