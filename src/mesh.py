from glcore import *
from array import array
import numpy

POSITION = "a_pos"
COLOR = "a_color"
NORMAL = "a_normal"
TEX_COORD = "a_tex_coord"
TANGENT = "a_tangent"

class Vertex:

	def __init__(self, position = [0, 0, 0], color = [1, 1, 1, 1], normal = [0, 0, 0], tex_coord = [0, 0], tangent = [0, 0, 0]):
		self.position = numpy.array(position, numpy.float32)
		self.color = numpy.array(color, numpy.float32)
		self.normal = numpy.array(normal, numpy.float32)
		self.tex_coord = numpy.array(tex_coord, numpy.float32)
		self.tangent = numpy.array(tangent, numpy.float32)

class Mesh:

	def __init__(self):
		self.vertex_buffer = None
		self.index_buffer = None
		self.shader = None
		self.hint = GL_DYNAMIC_DRAW
		self.vertices = array("f")
		self.indices = array("i")
		self.vert_count = 0
		self.index_count = 0
		self.__attrib_order = [POSITION, COLOR]
		
	def create(self):
		self.vertex_buffer = VertexBuffer()
		self.index_buffer = IndexBuffer()
		
		for x in self.__attrib_order:
			if x == POSITION:
				self.vertex_buffer.push_attrib(POSITION, 3)
			if x == COLOR:
				self.vertex_buffer.push_attrib(COLOR, 4)
			if x == NORMAL:
				self.vertex_buffer.push_attrib(NORMAL, 3)
			if x == TEX_COORD:
				self.vertex_buffer.push_attrib(TEX_COORD, 2)
			if x == TANGENT:
				self.vertex_buffer.push_attrib(TANGENT, 3)
				
	def delete(self):
		self.vertex_buffer.delete()
		self.index_buffer.delete()
	
	def add_vertices(self, vertices):
		for x in vertices:
			self.add_vertex(x)
			
	def add_vertex(self, vertex):
		self.vert_count += 1
		for x in self.__attrib_order:
			if x == POSITION:
				self.vertices.append(vertex.position[0])
				self.vertices.append(vertex.position[1])
				self.vertices.append(vertex.position[2])
			if x == COLOR:
				self.vertices.append(vertex.color[0])
				self.vertices.append(vertex.color[1])
				self.vertices.append(vertex.color[2])
				self.vertices.append(vertex.color[3])
			if x == NORMAL:
				self.vertices.append(vertex.normal[0])
				self.vertices.append(vertex.normal[1])
				self.vertices.append(vertex.normal[2])	
			if x == TEX_COORD:
				self.vertices.append(vertex.tex_coord[0])
				self.vertices.append(vertex.tex_coord[1])
			if x == TANGENT:
				self.vertices.append(vertex.tangent[0])
				self.vertices.append(vertex.tangent[1])
				self.vertices.append(vertex.tangent[2])	
				
	def add_indices(self, indices):
		for x in indices:
			self.add_index(x)
	
	def add_index(self, index):
		self.index_count += 1
		self.indices.append(index)
		return
		
	def add_face(self, i1, i2, i3):
		self.add_index(i1)
		self.add_index(i2)
		self.add_index(i3)
	
	def update(self):
		self.vertex_buffer.buffer_hint = self.hint
		self.index_buffer.buffer_hint = self.hint
		self.vertex_buffer.push_data(self.vertices, self.vert_count)
		self.index_buffer.push_data(self.indices)	
		return
		
	def render(self):
		self.shader.bind_vbo(self.vertex_buffer)
		self.shader.bind()
		
		self.vertex_buffer.render_indexed(self.index_buffer)
		
		self.shader.unbind()
		
	def clear(self):
		del self.vertices[:]
		del self.indices[:]
		
		self.vert_count = 0
		self.index_count = 0
		
		
		return
		